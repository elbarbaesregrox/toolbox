const express = require('express');
const router = express.Router();
const Producto = require('../models/producto');
//const { check, validationResult } = require('express-validator/check');

/* GET users listing. */
router.get('/', function(req, res, next) {

	Producto.find({}, function(err, productos) {
	    
	    res.send(productos);  
	});

});



router.get('/:id', function(req, res, next) {

	Producto.find({_id: req.params.id}, function(err, producto) {
	    
	    res.send(producto);  
	});

});



router.post('/', 

	function(req, res, next) {
			  	
		const newProducto = new Producto({
	  		descripcion: req.body.descripcion,
	  		precio: req.body.precio,
	  		cantidad: req.body.cantidad
		});


		newProducto.save(function(err) {
  	
		  	if (err) res.status(400).send();
  			
	  		console.log('Producto saved successfully!');

	  		res.status(201).send();
  		
		});

	}
);


router.delete('/:id',

	function(req, res, next) {

		 Producto.remove({_id: req.params.id}, function (err, producto) {
	            	
	            	if (err) {

	              		return console.error(err);
	            	}

	            	console.log('Producto deleted');

	            	res.status(201).send();
	    });
	}
);


module.exports = router;