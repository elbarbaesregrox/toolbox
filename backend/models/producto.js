var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductoSchema = new Schema({
  descripcion: { type: String, required: true, unique: true },
  precio: { type: Number, required: true },
  cantidad: { type: Number, required: true }
  
  
});

var Producto = mongoose.model('Producto', ProductoSchema);


module.exports = Producto;