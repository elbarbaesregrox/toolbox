import React, { Component } from 'react';
import Productos from './models/Productos'


class ProductoForm extends Component {

   constructor(props) {
       super(props);

       this.observer = props.observer;


       this.state = {descripcion: '', precio: '', cantidad:''};
       this.handleChange = this.handleChange.bind(this);
       this.handleSubmit = this.handleSubmit.bind(this);

   }


   
   handleChange(event) {

      let object= {};

       object[event.target.name] =  event.target.value;

       this.setState(object);
   }


   handleSubmit(event) {
      let self = this;
      Productos.post(this.state)
               .then(function(res) {
                     self.observer.publish("productoAdd")
                     self.setState((prevState, props) => {
                        return {descripcion:'', precio:'', cantidad:''};
                     });
               })
               .catch((err)=>alert("Ups!!! Hubo un Error al intentar agregar el producto!!!"));
      
      event.preventDefault();
   }


   render() {
      return (
         <div className="producto-form">
            <form className="form-inline" ref={(frm) => this.formProducto = frm}  onSubmit={this.handleSubmit}> 
            	<div className="form-group">
                  <label>Producto </label>
            		<input className="form-control" 
                        placeholder="Nombre del Producto"
                        type="text"
                        name="descripcion"
                        value={this.state.descripcion}
                        onChange={this.handleChange} />
            	
               </div>

               <div className="form-group">
            	                  <label>Precio </label>

	            	<input className="form-control"
                         placeholder="Precio"
                         type="text"
                         name="precio"
                         value={this.state.precio}
                         onChange={this.handleChange}
                          />
	            </div>
               <div className="form-group">
	               <label>Cantidad </label>

            		<input className="form-control"
                         placeholder="Stock disponible"
                         type="text"
                         name="cantidad"
                         value={this.state.cantidad}
                         onChange={this.handleChange}
                  />
               </div>
            	

            	<input type="submit" value="Agregar" className="btn btn-primary btn-add"/>
            </form>
            
         </div>
      );
   }
}

export default ProductoForm;
