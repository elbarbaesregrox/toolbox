import React, { Component } from 'react';
import ReactObserver from 'react-event-observer';

import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import './App.css'

import ProductoForm from './ProductoForm';
import ProductoList from './ProductoList';


class App extends Component {


  constructor(props){
      super(props);
      this.observer = ReactObserver();
  }
  render() {
     return (
            <div className="main">
              <div className="navbar navbar-fixed-top navbar-inverse">
                <div className="container-fluid">
                  <div className="navbar-header">
                    <a className="navbar-brand" href="#">Toolbox Evaluación</a>
                  </div>
                  <ul className="nav navbar-nav">
       
                  </ul>
                </div>
              </div>
              <div className="container-fluid">

                    <ProductoForm observer={this.observer}></ProductoForm>
                                    
                    <ProductoList observer={this.observer}></ProductoList>
                 
                
                
              </div>

               
            </div>
         
      );
   }
  
}

export default App;
