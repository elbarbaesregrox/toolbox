const axios = require('axios')

const Productos =  {}


 axios.defaults.crossDomain = true;

Productos.get = function () {

    return axios.get('http://localhost:4000/productos')
}


Productos.post = function (producto) {

    return axios.post('http://localhost:4000/productos',producto)
}


Productos.delete = function (id) {

    return axios.delete('http://localhost:4000/productos/'+id)
}

export default Productos;