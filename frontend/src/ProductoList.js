import React, { Component } from 'react';

import Productos from './models/Productos'

class ProductoList extends Component {
   
	constructor(props) {

    	super(props)
      this.observer = props.observer;


    	this.state = {'productos':[]}
    }



  handlerRefresh() {

      let self = this
      Productos.get().then(function (response) {
          console.log(response)
                 
            self.setState((prevState, props) => {
                return {productos: response.data};
            });
            
         })
         .catch(function (err) {
            console.log(err)
           
         });


  }  

  componentWillMount() {


    let self = this;
    this.observer.subscribe("productoAdd",()=>{
     
        self.handlerRefresh();
            
    });

 	  this.handlerRefresh();
    
 	}

  handlerDelete(id) {
    let self = this;
    Productos.delete(id).then((res)=>self.handlerRefresh());
    
  }

  render() {

    let self = this
    return (
      <div className='producto-list'>

        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Producto</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
         {this.state.productos.map(function (p) {

              return (
                  <tr key={p._id}>
                    <td>{p.descripcion}</td>
                    <td>{p.precio}</td>
                    <td>{p.cantidad}</td>
                    <td>
                       <button className="btn btn-primary" onClick={()=>self.handlerDelete(p._id)}>Eliminar</button>
                    </td>
                  </tr>
                  );
         })}
         </tbody>
         </table>
         </div>
        
        
      </div>
    );
  }
}

export default ProductoList;
